(use coops coops-encore test)

(define-class* (self <point>) ()
  ((x 0) (y 0))
  ((add (p <point>))
   (make <point>
     'x (+ (x self) (x p))
     'y (+ (y self) (y p)))))


(define point (make <point> 'x 10 'y 20))

(test (make <point> 'x 20 'y 30)
      (add point (make <point> 'x 10 'y 10)))

(define-record-class (3d-point <3d-point>) (<point>)
  ((x 0) (y 0) (z 0))
  ((add (p <3d-point>))
   (make-3d-point (+ (x 3d-point) (x p))
                  (+ (y 3d-point) (y p))
                  (+ (z 3d-point) (z p)))))

(define 3d (make-3d-point))

(test-assert (3d-point? 3d))
(test 0 (slot-value 3d 'x))
(3d-point-x-set! 3d 2)
(test 2 (slot-value 3d 'x))

(test (make-3d-point 5 7 9)
      (add (make-3d-point 1 2 3)
           (make-3d-point 4 5 6)))

(test 10 (3d-point-x (make-3d-point 10 20 30)))
(test (make <point> 'x 2 'y 2)
      (add (make <3d-point> 'x 1 'y 1)
           (make <point>    'x 1 'y 1)))
