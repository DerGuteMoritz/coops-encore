(module coops-encore

(define-class* define-record-class)

(import chicken scheme)

(use coops)

(require-library matchable)

(import-for-syntax matchable)

(define-syntax define-class*
  (ir-macro-transformer
   (lambda (x i c)
     (match x
       ((_ (self <class>) (parents ...) (slots ...) methods ...)
        (let* ((slot-names (map (lambda (s)
                                  (if (pair? s)
                                      (car s) s))
                                slots))
               (slot-names* (map gensym (map strip-syntax slot-names))))
          `(begin
             (define-class ,<class> ,parents ,slots)

             ,@(map (lambda (slot-name slot-name*)
                      (let ((slot-value `(slot-value x ',slot-name)))
                        `(define ,slot-name*
                           (getter-with-setter
                            (lambda (x)
                              ,slot-value)
                            (lambda (x y)
                              (set! ,slot-value y))))))
                    slot-names slot-names*)

             . ,(map (match-lambda
                      (((name args ...) body ...)
                       `(define-method (,name (,self ,<class>) . ,args)
                          (let ,(map list slot-names slot-names*)
                            . ,body))))
                     methods))))))))

(define-syntax define-record-class
  (ir-macro-transformer
   (lambda (x i c)
     (match x
       ((_ (name <class>) rest ...)
        `(define-record-class (,name ,name ,<class>) . ,rest))
       ((_ (self name <class>) (parents ...) (slots ...) methods ...)
        (let* ((name* (symbol->string (strip-syntax name)))
               (slot-names  (map (lambda (s)
                                   (if (pair? s) (car s) s))
                                 slots))
               (getters     (map (lambda (slot-name)
                                   (string-append
                                    name* "-" (symbol->string (strip-syntax slot-name))))
                                 slot-names))
               (setters     (map (lambda (g)
                                   (string->symbol (string-append g "-set!")))
                                 getters))
               (getters     (map string->symbol getters))
               (predicate   (string->symbol (string-append name* "?")))
               (constructor (string->symbol (string-append "make-" name*))))
          `(begin
             (define-class* (,self ,<class>)
               ,parents ,slots . ,methods)
             (define (,(i predicate) x)
               (subclass? (class-of x) ,<class>))
             (define (,(i constructor) . args)
               (apply make ,<class>
                      (append-map (lambda (s a)
                                    (list s a))
                                  ',slot-names
                                  args)))
             ,@(map (lambda (name slot)
                      `(define (,(i name) x)
                         (slot-value x ',slot)))
                    getters slot-names)
             ,@(map (lambda (name slot)
                      `(define (,(i name) x y)
                         (set! (slot-value x ',slot) y)))
                    setters slot-names))))))))


)
